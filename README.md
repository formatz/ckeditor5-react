Format-Z [CKEditor 5](https://ckeditor.com/ckeditor-5/) React component.

This is a fork of [official CKEditor-React](https://www.npmjs.com/package/@ckeditor/ckeditor5-react) v1.0.0-beta.1 component with a new props onFzAction which listen a CKEditor event call fzAction. 
This event can be used by plugins and will be fire when the button will be clicked from the CKEditor toolbar.

## Documentation

See the [React component](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/react.html) article in the [CKEditor 5 documentation](https://ckeditor.com/docs/ckeditor5/latest) website.

## Install
```
yarn add -D ssh:git@gitlab.com:formatz/ckeditor5-react.git#1.0.0
```

Replace 1.0.0 by the current TAG.
